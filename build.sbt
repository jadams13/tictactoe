name := "tictactoe"

version := "1.0"

scalaVersion := "2.11.8"


libraryDependencies += "org.scalafx" %% "scalafx" % "8.0.92-R10"

libraryDependencies += "com.netflix.rxjava" % "rxjava-scala" % "0.20.7"


unmanagedJars in Compile += Attributed.blank(file(System.getenv("JAVA_HOME") + "/jre/lib/ext/jfxrt.jar"))

mainClass in assembly := some("com.github.joeadams.HelloSBT")

assemblyJarName in assembly := "tictactoe.jar"

