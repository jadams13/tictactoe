package com.github.joeadams

import scalafx.scene.control.TableView
import scalafx.scene.layout.{ColumnConstraints, GridPane, RowConstraints}

/**
  * Created by joe on 7/6/16.
  */
object Board extends GridPane{
  val columnConstraint=new ColumnConstraints(){
    percentWidth=(100/33.toDouble)
    minWidth=200
  }
  val rowConstraint=new RowConstraints(){
    percentHeight=(100/33.toDouble)
    minHeight=200
  }
  columnConstraints=List.tabulate(3)(_=>columnConstraint)
  rowConstraints=List.tabulate(3)(_=>rowConstraint)
  gridLinesVisible=true

}
