package com.github.joeadams




import com.github.joeadams.ObservableNode.Push
import rx.lang.scala.subjects.PublishSubject

import scalafx.Includes._
import scalafx.scene.control.{Button, Label, TableView}
import scalafx.Includes._
import scalafx.event.EventHandler
import scalafx.scene.Node
import javafx.scene.input.MouseEvent
import javafx.event.EventHandler

import rx.lang.scala.Notification.OnNext
import rx.lang.scala.{Notification, Observable}
/**
  * Created by joe on 7/7/16.
  */
object ObservableNode {
  sealed case class Push()
  val push=Push()


  implicit def makeNodeObservable(node:Node)=new ObservableNode(node)
}

class ObservableNode(node:Node){
  val pubsub =PublishSubject[Notification[Push]]()
  val handler=new EventHandler[MouseEvent]{
    def handle(e:MouseEvent):Unit={
      pubsub.onNext(OnNext(ObservableNode.push))
    }
  }
  node.addEventHandler(MouseEvent.MOUSE_CLICKED,handler)

  def getObservable= pubsub








}
