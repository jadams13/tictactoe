package com.github.joeadams
import rx.lang.scala.Notification.{OnCompleted, OnError, OnNext}

import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.geometry.{Insets, Pos}
import scalafx.scene.Scene
import scalafx.scene.control.{Button, Label, TableView}
import scalafx.scene.layout.{AnchorPane, BorderPane, HBox, VBox}
import scalafx.scene.text.{Font, Text, TextAlignment}
import scalafx.geometry
/**
  * Created by joe on 7/6/16.
  */
object TicTacToe  extends JFXApp {

  //I'm pulling the implicit in
  import ObservableNode.makeNodeObservable


  val sceneRoot=new VBox(){
    fillWidth=true
  }
  val title=new HBox(){
    children=Seq(
      new Text() {
        text = "TIC TAC TOE!"
        font = new Font(size = 50)
        textAlignment = TextAlignment.Center
        alignmentInParent = Pos.Center
      }
    )
    alignment=Pos.Center
  }

  val playerIsX=new Button(
    text="I want to be X!"
  )

  playerIsX.getObservable.subscribe(n=>n match{
    case OnNext(x)=>println("x")
    case OnError(err)=>println(s"err: $err")
    case onComplete =>println("Done!")
  })

  val playerIsO=new Button(
    text="I want to be O!"
  )



  val wannaPlay=new HBox(){
    alignment=Pos.Center
    children=Seq(playerIsX,playerIsO)
  }
  val quit=new Button(text="I quit!")
  def mutateSceneRoot():Unit={
    sceneRoot.children=Seq(title,wannaPlay,Board)
  }


  mutateSceneRoot()


  stage = new PrimaryStage {
    height=800
    width=600
    scene = new Scene {

      //We will mutate the children of sceneRoot
      root=sceneRoot
    }


  }






}
